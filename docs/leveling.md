# Player leveling system

Each SteemCraft player starts with level zero. Every two levels players gains half heart.

You can calculate the amount of experience you need to the next level using the formula:

```
(next_level^2 * 256) - (current_level^2 * 256)
```

The result is that you need 256 experience points on the first level and 768 on the second level.


In the case of enchanting and repairing items, you do not lose your experience level, but you still need to own it (which means that you have a long way to level 30).



# Monster leveling system

The level of monsters depends mainly on the distance from the spawn and the type of map (The Overworld, Nether and End).

On each of them a different range of levels is set:

```
The Overworld: 1-20 level
The Nether: 20-40 level
The End: 40-60 level
```

From the level of the monster depends on his health, possible equipment and additional effects.

Monster health can be calculated from the formula: (with a few exceptions)

```
monster_level + 1
```

Exceptions:

```
Ghast: monster_level * 3
Enderman: monster_level * 4
```